## COMP.SE.140: Continuous Development and Deployment - DevOps

Each exerciese is in its own branch, but a copy is also in the main-branch
in a separate directory. E.g.

main:
- vh6\_ansible

Branch 'ansible' has the same files as in vh6\_ansible but directly in root 
dir.

# How to create new completely empty branch for new task
```
git checkout --orphan messaging
git rm -rf .
git commit --allow-empty -m "Init for Messaging"
git push origin messaging
```
