// Elias Meyer, 01.11.2022
// COMP.CE.140

use amqp::{Session, AMQPError, Table, TableEntry, Basic, protocol, Channel};
use amqp::TableEntry::LongString;

// For sleep
use std::{thread, time};

use crate::rabbitmq_schema::declare_queues;
use crate::rabbitmq_schema::declare_routing;

const DELAY_MS: u64 = 1000;
const QUEUE_NAME: &str = "queue_intermediate";

const AMQP_HOST: &str = "amqp://rabbitmq//";

const EXCHANGE_NAME: &str = "test_exchange";
const ROUTING_KEY_INTERMEDIATE: &str = "compse140.i";

struct MyConsumer {
    deliveries_number: u64
}

impl amqp::Consumer for MyConsumer {
    fn handle_delivery(&mut self, 
            channel: &mut Channel, 
            deliver: protocol::basic::Deliver, 
            _headers: protocol::basic::BasicProperties, 
            body: Vec<u8>)
    {
        let prev_msg = String::from_utf8(body)
            .unwrap_or("<no message>"
            .to_string());

        let new_msg: String = format!("Got {}", prev_msg);

        thread::sleep(time::Duration::from_millis(DELAY_MS));

        println!("[struct] {}", new_msg);

        self.deliveries_number += 1;
        channel.basic_ack(deliver.delivery_tag, false).unwrap();

        let mut headers = Table::new();
        let field_array = vec![
            TableEntry::LongString("Foo".to_owned()),
            TableEntry::LongString("Bar".to_owned())
        ];
        headers.insert(
            "foo".to_owned(),
            TableEntry::LongString("Foo".to_owned())
        );
        headers.insert(
            "field array test".to_owned(),
            TableEntry::FieldArray(field_array)
        );
        let properties = protocol::basic::BasicProperties {
            content_type: Some("text".to_owned()),
            headers: Some(headers), ..Default::default()
        };

        channel.basic_publish(
            EXCHANGE_NAME,
            ROUTING_KEY_INTERMEDIATE,
            true,
            false,
            properties,
            (new_msg.as_bytes()).to_vec()
        ).ok().expect("Failed publishing");
    }
}

pub fn start_intermediate() {

    let mut props = Table::new();
    props.insert("example-name".to_owned(), LongString("consumer".to_owned()));

    //****************************
    // Connect to server
    //****************************

    let mut session: Session;
    // TODO: Consider 
    // const NUM_RETRIES: usize = 50
    // for attempt_idx in 0..NUM_RETRIES { ... }
    // println!("Failed attempt {}", attempt_idx)
    loop {
        let session_tmp: Result<Session, AMQPError> = 
            Session::open_url(AMQP_HOST);
        match session_tmp {
            Ok(v) => {
                session = v;
                break;
            }
            Err(_e) => println!("Could not connect to AMQP server"),
        }
        // Sleep by thread::sleep
        thread::sleep(time::Duration::from_millis(DELAY_MS));
    }

    //****************************
    // Create queues and routing
    //****************************

    let mut channel = session.open_channel(1).ok().expect("Error openning channel 1");
    println!("Opened channel: {:?}", channel.id);

    declare_queues(&mut channel);

    declare_routing(&mut channel);

    // Emtpy queue
    channel.basic_prefetch(100).ok().expect("Failed to prefetch");

    println!("Declaring consumers...");

    let my_consumer_1 = MyConsumer { deliveries_number: 0 };

    // exclusive: bool, nowait: bool, arguments: Table
    let consumer_name_1 = channel.basic_consume(
        my_consumer_1, 
        QUEUE_NAME,
        "",
        false,
        false,
        false,
        false,
        Table::new()
    );

    println!("Starting consumer {:?}", consumer_name_1);

    //****************************
    // Listen
    //****************************

    channel.start_consuming();
}
