
use warp::Filter;

#[tokio::main]
async fn main() {
    let messages_file_path: &str = "../messages/messages.txt";
    let route = warp::path::end().and(warp::fs::file(messages_file_path));

    warp::serve(route).run(([0, 0, 0, 0], 8080)).await;
}
