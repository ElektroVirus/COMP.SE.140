#!/bin/bash
set -euo pipefail

podman rm --force rabbitmq
podman run -d                   \
    --name rabbitmq             \
    --hostname my-rabbit        \
    -p 5672:5672                \
    -p 15672:15672              \
    rabbitmq:3-management

