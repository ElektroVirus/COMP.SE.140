// Elias Meyer, 01.11.2022
// COMP.CE.140
//
// Example taken from
// https://github.com/Antti/rust-amqp/blob/master/examples/producer.rs

mod observer;
mod rabbitmq_schema;

use observer::start_observer;


fn main() {
    env_logger::init().unwrap();
    start_observer();
}

