// Elias Meyer, 01.11.2022
// COMP.CE.140

use amqp::{Session, AMQPError, Table, Basic, protocol, Channel};
use amqp::TableEntry::LongString;

// For timestamp
use chrono::prelude::{DateTime, Utc};

// For sleep
use std::{thread, time};

// For file
use std::fs::OpenOptions;
use std::fs::File;
use std::io::Write;
use std::fs;

// For Path
use std::path::Path;

use crate::rabbitmq_schema::declare_queues;
use crate::rabbitmq_schema::declare_routing;

const DELAY_MS: u64 = 1000;
const QUEUE_NAME_1: &str = "queue_observer";

const MESSAGE_FILENAME: &str = "../messages/messages.txt";
const AMQP_HOST: &str = "amqp://rabbitmq//";


fn iso8601(st: &std::time::SystemTime) -> String {
    let dt: DateTime<Utc> = st.clone().into();
    // formats like "2001-07-08T00:34:60.026490+09:30"
    // format!("{}", dt.format("%+"))

    // 2022-10-01T06:35:01.373Z 2 MSG_1 to compse140.o
    format!("{}", dt.format("%FT%T%.3fZ"))
}

struct MyConsumer {
    deliveries_number: u64
}

impl amqp::Consumer for MyConsumer {
    fn handle_delivery(&mut self, 
            channel: &mut Channel, 
            deliver: protocol::basic::Deliver, 
            _headers: protocol::basic::BasicProperties, 
            body: Vec<u8>)
    {
        // Example printout
        // 2022-10-01T06:35:01.373Z 2 MSG_1 to compse140.o
        let msg = String::from_utf8(body)
            .unwrap_or("<no message>"
            .to_string());
        let res: String = 
            format!("{} {} {} to {}", 
                    iso8601(&std::time::SystemTime::now()), 
                    self.deliveries_number, 
                    msg, 
                    deliver.routing_key
            );


        //****************************
        // Write message to file
        //****************************

        // Open a file in write-only (ignoring errors).
        let mut file_ref = OpenOptions::new()
            .append(true)
            .open(MESSAGE_FILENAME)
            .expect("Unable to open file");

        // Write a &str in the file (ignoring the result).
        // writeln!(&mut file, "Hello World!").unwrap();
        writeln!(&mut file_ref, "{}", res).unwrap();
        
        println!("[struct] {}", res);

        self.deliveries_number += 1;
        channel.basic_ack(deliver.delivery_tag, false).unwrap();
    }
}

pub fn start_observer() {

    let mut props = Table::new();
    props.insert("example-name".to_owned(), LongString("consumer".to_owned()));


    //****************************
    // Connect to server
    //****************************

    let mut session: Session;
    // TODO: Consider 
    // const NUM_RETRIES: usize = 50
    // for attempt_idx in 0..NUM_RETRIES { ... }
    // println!("Failed attempt {}", attempt_idx)
    loop {
        let session_tmp: Result<Session, AMQPError> = 
            Session::open_url(AMQP_HOST);
        match session_tmp {
            Ok(v) => {
                session = v;
                break;
            }
            Err(_e) => println!("Could not connect to AMQP server"),
        }
        // Sleep by thread::sleep
        thread::sleep(time::Duration::from_millis(DELAY_MS));
    }


    //****************************
    // Open file for writing
    //****************************

    // Create path so the parent dir can be created easily
    let messages_path = Path::new(MESSAGE_FILENAME);

    // Create new file - empty file if it exists already
    match fs::create_dir(messages_path.parent().unwrap()) {
        Ok(_ok) => {},
        Err(_e) => {},
    }
    File::create(messages_path).unwrap();


    //****************************
    // Create queues and routing
    //****************************

    let mut channel = session.open_channel(1).ok().expect("Error openning channel 1");
    println!("Opened channel: {:?}", channel.id);

    declare_queues(&mut channel);

    declare_routing(&mut channel);

    // Emtpy queue
    channel.basic_prefetch(100).ok().expect("Failed to prefetch");

    println!("Declaring consumers...");

    let my_consumer_1 = MyConsumer { deliveries_number: 1 };

    // exclusive: bool, nowait: bool, arguments: Table
    let consumer_name_1 = channel.basic_consume(
        my_consumer_1, 
        QUEUE_NAME_1,
        "",
        false,
        false,
        false,
        false,
        Table::new()
    );

    println!("Starting consumer {:?}", consumer_name_1);


    //****************************
    // Listen
    //****************************

    channel.start_consuming();
}
