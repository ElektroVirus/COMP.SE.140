// Elias Meyer, 01.11.2022
// COMP.CE.140
//
// Example taken from
// https://github.com/Antti/rust-amqp/blob/master/examples/producer.rs

mod original;
mod rabbitmq_schema;

use original::start_original;

// For ^C killings
use std::sync::Arc;
use std::sync::atomic::AtomicBool;
use signal_hook::flag;
use signal_hook::consts::TERM_SIGNALS;

fn main() {
    // Make sure double CTRL+C and similar kills
    let term_now = Arc::new(AtomicBool::new(false));
    for sig in TERM_SIGNALS {
        flag::register_conditional_shutdown(*sig, 1, Arc::clone(&term_now)).unwrap();
        flag::register(*sig, Arc::clone(&term_now)).unwrap();
    }

    start_original();
}
