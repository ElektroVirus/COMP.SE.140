use std::env;

use amqp::{Session, AMQPError, Table, Basic, TableEntry, protocol};
use std::default::Default;

// For sleep
use std::{thread, time};

const LIMIT: i32 = 3;
const BOOT_DELAY_MS: u64 = 1000;
const SEND_DELAY_MS: u64 = 3000;

const AMQP_HOST: &str = "amqp://rabbitmq//";

const EXCHANGE_NAME: &str = "test_exchange";

use crate::rabbitmq_schema::declare_queues;
use crate::rabbitmq_schema::declare_routing;

pub fn start_original() {
    let args: Vec<String> = env::args().collect();
    let default: String = String::from("default");
    let queue_arg: String = args.get(1).unwrap_or(&default).to_string();
    // routing key == topic
    let routing_key: &str = &queue_arg;
    let exchange_name: &str = EXCHANGE_NAME;

    println!("Using queue {}", routing_key);

    let mut session: Session;
    loop {
        let session_tmp: Result<Session, AMQPError> = 
            Session::open_url(AMQP_HOST);
        match session_tmp {
            Ok(v) => {
                session = v;
                break;
            }
            Err(_e) => println!("Could not connect to AMQP server"),
        }
        // Sleep by thread::sleep
        thread::sleep(time::Duration::from_millis(BOOT_DELAY_MS));
    }

    //****************************
    // Create queues and routing
    //****************************

    let mut channel = session.open_channel(1).ok().expect("Error openning channel 1");
    println!("Opened channel: {:?}", channel.id);

    declare_queues(&mut channel);
    declare_routing(&mut channel);

    let mut counter: i32 = 1;
    loop {
        let message: String = format!("MSG_{}", counter);

        let mut headers = Table::new();
        let field_array = vec![
            TableEntry::LongString("Foo".to_owned()),
            TableEntry::LongString("Bar".to_owned())
        ];
        headers.insert(
            "foo".to_owned(),
            TableEntry::LongString("Foo".to_owned())
        );
        headers.insert(
            "field array test".to_owned(),
            TableEntry::FieldArray(field_array)
        );
        let properties = protocol::basic::BasicProperties {
            content_type: Some("text".to_owned()),
            headers: Some(headers), ..Default::default()
        };

        channel.basic_publish(
            exchange_name,
            routing_key,
            true,
            false,
            properties,
            (message.as_bytes()).to_vec()
        ).ok().expect("Failed publishing");

        println!("Published message {} to exchange {} with routing key {}", 
                 message, exchange_name, routing_key);

        if counter >= LIMIT {
            break;
        }
        counter += 1;

        // Sleep by thread::sleep
        thread::sleep(time::Duration::from_millis(SEND_DELAY_MS));
    }

    match channel.close(200, "Bye") {
        Ok(_ok) => {},
        Err(_e) => {},
    }
    session.close(200, "Good Bye");

    // Don't exit, start waiting
    loop {}
}
