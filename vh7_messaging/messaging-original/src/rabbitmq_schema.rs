use amqp::{QueueBuilder, Table};

const EXCHANGE_NAME: &str = "test_exchange";
const EXCHANGE_TYPE: &str = "topic";

const QUEUE_NAME_1: &str = "queue_observer";
const QUEUE_NAME_2: &str = "queue_intermediate";

pub fn declare_queues(channel: &mut amqp::Channel) {
    let exchange_declare = channel.exchange_declare(EXCHANGE_NAME, EXCHANGE_TYPE,
        false, true, false, false, false, Table::new());
    match exchange_declare {
        Ok(ref _ok) => {},
        Err(e) => panic!("Could not declare exhange {EXCHANGE_NAME}: {}", e),
    }

    let queue_builder_1 = QueueBuilder::named(QUEUE_NAME_1).durable();
    let queue_declare_1 = queue_builder_1.declare(channel);
    match queue_declare_1 {
        Ok(ref _ok) => {},
        Err(e) => panic!("Could not declare channel {QUEUE_NAME_1}: {}", e),
    }
    println!("Queue declare: {:?}", queue_declare_1);

    let queue_builder_2 = QueueBuilder::named(QUEUE_NAME_2).durable();
    let queue_declare_2 = queue_builder_2.declare(channel);
    match queue_declare_2 {
        Ok(ref _ok) => {},
        Err(e) => panic!("Could not declare channel {QUEUE_NAME_2}: {}", e),
    }
    println!("Queue declare: {:?}", queue_declare_2);
}

pub fn declare_routing(channel: &mut amqp::Channel) {
    // TODO: Is in the nowait inversion bug? nowait = true: program hangs, nowait = false: program
    // does not hang

    // Observer gets all messages on network
    let queue_bind_1 = channel.queue_bind(
        QUEUE_NAME_1,
        EXCHANGE_NAME,
        "compse140.#",
        false, Table::new()
    );
    match queue_bind_1 {
        Ok(ref _ok) => {},
        Err(e) => panic!("Could not bind: {e}"),
    }
    println!("Queue #1 bound: {:?}", queue_bind_1);

    // Intermediate gets only messages from topic compse140.o
    let queue_bind_2 = channel.queue_bind(
        QUEUE_NAME_2,
        EXCHANGE_NAME,
        "compse140.o",
        false,
        Table::new()
    );

    match queue_bind_2 {
        Ok(ref _ok) => {},
        Err(e) => panic!("Could not bind: {e}"),
    }
    println!("Queue #2 bound: {:?}", queue_bind_2);
}
