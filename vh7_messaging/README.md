# AMQP Exercise

## 🐰MQ

I used this RabbitMQ implementation for Rust:

https://docs.rs/amqp/0.1.3/amqp/
https://github.com/Antti/rust-amqp

The library is quite all right, however a few things are not implemented, e.g. this
https://github.com/Antti/rust-amqp/blob/master/src/channel.rs#L352 which would have been
handy in checking whether the messages are received correctly. But this is not a problem,
my implementation does not depend on which service will start first: each one of them 
will create the same topics / queues / exchange / routing so messages are never lost.

This implementation does *not* use simple delay for waiting until RabbitMQ is up, instead 
the amqp-library returns a result with AMQPError, which is checked for. If the result is 
erroneus, one second is waited after which the connection is retried. Once the connection 
succeeds, each service (Original, Intermediate, Observer) will publish the schema (queues, 
exchange and the routing) to the RabbitMQ which is declared in `rabbitmq_shema.rs`.

In my implementation the `rabbitmq_schema.rs` is copied across the projects but in real
life it would *be* the same file, for it to be more easily maintainable. 

## System

```
Linux Fedora 36 6.0.5-200.fc36.x86_64 #1 SMP PREEMPT_DYNAMIC Wed Oct 26 15:55:21 UTC 2022 x86_64 x86_64 x86_64 GNU/Linux
Docker version 20.10.21, build baeda1f
docker-compose version 1.29.2, build unknown
```

## Benefits of Topic-based communication

HTTP is a server-client -protocol which means one client is able to communicate with one server
efficiently at a time. This restriction comes directly from how TCP works on top of which HTTP
is built. 

On more complex systems like this, however, the overhead from using HTTP to communicate securely 
would be tremendous. Messaging-oriented-middleware has multiple pros. For one, the receiving end 
does not have to be on - messages will be cached on the messaging server, in this case the 
RabbitMQ-container. Also, the receiver can acknowledge the messages when it sees fit: the messages
can be stored on the server until it is clear the receiver does not need them anymore. So 
technically it may pull the messages multiple times.

The messages may be sent also to multiple receivers. If the sender it a slow, low-power 
microcontroller running on a battery, it does not need to repeat the message multiple times. It's 
enought it posts the messages only once and the messaging server handles the broadcasting - AMQP 
uses the term fanout for this type of routing.

More complex situations require message routing by topics. For instance, Unix logging tool `syslog` 
routes logs based on both severity (info/warn/crit...) and facility (auth/cron/kern...). This 
multi-dimensional routing is the business of topic based routing, which is one part of RabbitMQ.

Back to the request-response based communication like HTTP, all previous situations would require
the writing of AMQP again.

Source: https://www.rabbitmq.com/tutorials/tutorial-five-python.html

## Learnings

- RabbitMQ and AMQP (the benefits of topic-based communication)
- Rust 

I'm happy I have been doing the exercieses on this course with Rust. I'm quite unexperienced in 
Rust but if I don't write something with it I'll never learn it. And I want to learn it, I want
to be able to use it so effectively I can work with it with ease. I want to replace C/C++ and 
Rust seems at this point the best alternative.

I am glad there existed this AMQP-library for Rust, because with it I would not have been able 
to write this application with it. It seems that the library has not been developed anymore since 
2019, and it still has a few missing properties but it was good enough for this.

I also learned how to use topic-based communication. I have once made a group assignment (2020) 
with Python and MQTT, but this exerciese really helped to understand the message routing.


