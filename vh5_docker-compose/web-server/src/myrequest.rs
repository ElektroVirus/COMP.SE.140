
pub async fn myrequest() -> String {
    const URL: &str = "http://service2:3002/";

    match reqwest::get(URL).await {
        Ok(response) => {
            match response.text().await {
                Ok(text) => text.to_string(),
                Err(_) => "Request text conversion failed.".to_string()
            }
        }
        Err(_) => "Request failed".to_string()
    }
}
