use std::net::SocketAddr;
use warp::{Filter};

use local_ip_address::local_ip;

const HEADER_HOST: &str = "Host";

pub fn ipinfo() 
    -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {

    let base = warp::path("ipinfo");

    let get = base
        .and(warp::get())
        .and(warp::path::end())
        .and(warp::addr::remote())
        .and(warp::header::<String>(HEADER_HOST))
        .and_then(ipinfo_get);

    get
}

async fn ipinfo_get(remote: Option<SocketAddr>, host: String) 
    -> Result<String, warp::Rejection> {

    // How am I supposed to know my local IP-address? Server listens on 
    // 127.0.0.1:8080, but in docker-compose, where the containers do have 
    // separate IP-addresses I probably have to use 0.0.0.0:8080 because 
    // otherwise the requests must come from localhost.
    //
    // The correct way would be to find out Host-header, make a reverse 
    // DNS query with it to find out the local outbound IP address.
    //
    // Other solution could also be to run `ip addr show eth0` or similar
    // to find out the IP address of the interface.
    //
    // let local_socket: &str = "127.0.0.1:8080";

    let my_local_ip = local_ip().unwrap();
    println!("This is my local IP address: {:?}", my_local_ip);

    match remote {
        Some(val) => Ok(format!("Hello from {:?}\nto {}\n", val, host)),
        None => panic!("Not a socket connection"),
    }
}

// “Hello from “ + <Remote IP address + port of incoming Request1 1 separated with colon (:)>
// “ to “ + <Local IP address and port of Service1 separated with colon (:)>
