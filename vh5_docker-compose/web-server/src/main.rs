// Response format
// “Hello from “ + <Remote IP address + port of incoming Request1 1 separated with colon (:)>
// “ to “ + <Local IP address and port of Service1 separated with colon (:)>

mod myserver;
mod myrequest;

use std::env;
use myserver::setup;

#[tokio::main]
async fn main(){
    let args: Vec<String> = env::args().collect();
    
    let default: String = String::from("default"); 
    let port_arg: String = args.get(1).unwrap_or(&default).to_string();

    setup(port_arg).await;

}
