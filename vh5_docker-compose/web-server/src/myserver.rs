
use std::convert::Infallible;
use hyper::{Body, Request, Response, Server};
use hyper::server::conn::AddrStream;
use hyper::service::{make_service_fn, service_fn};
use std::net::SocketAddr;

use crate::myrequest::myrequest;

pub async fn setup(port_arg: String) {

    let port_arg_str: &str = &port_arg;

    let port: u16 = match port_arg_str {
        "service1"  => 3001,
        "service2"  => 3002,
        _           => 3000
    };

    let recursive_request = port_arg_str == "service1";

    let addr = ([0, 0, 0, 0], port).into();
    println!("Running server at {}", addr);
    
    let make_svc = make_service_fn(|socket: &AddrStream| {

        let remote_addr: SocketAddr = socket.remote_addr();
        let local_addr:  SocketAddr = socket.local_addr();

        async move {
            Ok::<_, Infallible>(service_fn(move |_: Request<Body>| async move {
                Ok::<_, Infallible>(
                    Response::new(Body::from(
                        response(recursive_request, remote_addr, local_addr).await
                    ))
                )
                
            }))
        }
    });

    // Then bind and serve...
    let server = Server::bind(&addr)
        .serve(make_svc);

    // Finally, spawn `server` onto an Executor...
    if let Err(e) = server.await {
        eprintln!("server error: {}", e);
    }

}

async fn response(recursive_request: bool, remote: SocketAddr, local: SocketAddr) -> String {
    if recursive_request {
        let result = myrequest().await;
        format!("Hello from {} \nto {}\n{result}", remote, local)
    }
    else {
        format!("Hello from {} \nto {}\n", remote, local)
    }
}
