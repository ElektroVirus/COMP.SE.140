
This is the webserver for both containers.

If run with argument 'service2' (like cargo run service2), the service 
is run as the inner container. It will simply reply to incoming requests 
listening at 127.0.0.1:3002.

If run with argument 'service1' (like cargo run service1), the project will 
make a request to 127.0.0.1:3002 where service2 should be running. The service1
will reply to a request with the combined reply of itself and of service2.
The service is listening at 127.0.0.1:3001.

Running the prgogram without additional arguments or with something else, 
the service is listening at 127.0.0.1:3000 and will only reply to incoming 
requests like service2.

# Testing with Podman

## Create network
```
podman network create devopsnet
```

## Build and run service1
```
podman build -t service1 -f Dockerfile_service1
podman run --init -ti --rm -p 8001:3001 --name service1 --net devopsnet localhost/service1:latest
```

## Build and run service2
```
podman build -t service2 -f Dockerfile_service2
podman run --init -ti --rm --name service2 --net devopsnet localhost/service2:latest
```

## Test connection
```
curl localhost:8001
```


# Usage with docker-compose

## Setup container
```
docker-compose up --build
```

## Test connection
```
curl localhost:8001
```

# Report

## Explanation 

```
$ curl 127.0.0.1:3001
Hello from 127.0.0.1:56972
to 127.0.0.1:3001
Hello from 127.0.0.1:58794
to 127.0.0.1:3002
```

The two containers run in the same network. The first service listens on port
3001 and service2 on port 3002. service1 port 3001 is published to the host 
port 8001.

When a request is made to the localhost:8001, it is passed to the first 
container, port 3001. The first line of the response '127.0.0.1:56972' 
indicates the source socket - the port 56972 was chosen randomly. 

When the first container and its web server receives the request, it will make 
a request to the second container to the address 'http://service2:3002'. The 
Docker Compose creates a network with working dns, where the container name 
can be used in an URL. In this case, the reqeuest is sent from 
'127.0.0.1:58794' to 127.0.0.1:3002 (again the source port is chosen randomly).

The web server was implemented using Rust. I started implementing this 
exercise with web server named Warp, since it is a nice high level web framework 
for creating web servers and REST-APIs. However, the requirement about 
destination address was not possible to get in Warp, which is why I swithed to 
a lower level http-implementation in Rust, Hyper. Warp is actually build using
Hyper. I don't know why the course wanted to know the destination socket, since
it should be the same socket, on which the web server is listening on.

## docker container ls

```
$ docker container ls
CONTAINER ID   IMAGE                 COMMAND                 CREATED          STATUS          PORTS                                       NAMES
0b9ed02106c2   web-server_service1   "web-server service1"   45 minutes ago   Up 45 minutes   0.0.0.0:8001->3001/tcp, :::8001->3001/tcp   web-server_service1_1
ea107e30b8e0   web-server_service2   "web-server service2"   45 minutes ago   Up 45 minutes                                               web-server_service2_1
```

## docker network ls

```
$ docker network ls
NETWORK ID     NAME                 DRIVER    SCOPE
5193a3fdd70f   bridge               bridge    local
817eefe0ff88   host                 host      local
012cb131dfea   none                 null      local
5b8a7391d5a9   web-server_default   bridge    local
```
