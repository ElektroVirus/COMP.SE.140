# Ansible

## SSH using keyfile

Dockerfile copies authorized_keys file into /home/ssluser/.ssh/authorized_keys,
chowns and chmods it accordingly. So put your ssh-key into meyer/ansible/authorized_keys.

## Uptime

I learnt that uptime inside container shows hosts's uptime. Probably because 
it comes directly from kernel and the container is just a 'linux namespace 
on steroids', so essentially it's just a filesystem. It uses the hosts kernel.
That's why it shows the uptime of the host kernel.

## Development

As you can see from ```deploy_ansible-container.sh```, I used podman instead of 
docker (podman is the default on Fedora Linux). One advantage is, it does not 
need a root service like Docker does. However, when run rootless, it can't 
allocate own IP-addresses for the containers. That's why I mapped the port 22 
to host ports 2222 and 2223.


## Report

Ansible prints nice ascii-arts if package Cowsay is installed. The image is 
random if environment variable ```export ANSIBLE_COW_SELECTION=random``` is set :D
The Dragon (O3 last art) is my favourite.

### O1

```
./run.sh ansible-test/setup.yml

 ________________________
< PLAY [ansible-test] >
 ---------------------
    \
     \
                                   .::!!!!!!!:.
  .!!!!!:.                        .:!!!!!!!!!!!!
  ~~~~!!!!!!.                 .:!!!!!!!!!UWWW$$$
      :$$NWX!!:           .:!!!!!!XUWW$$$$$$$$$P
      $$$$$##WX!:      .<!!!!UW$$$$"  $$$$$$$$#
      $$$$$  $$$UX   :!!UW$$$$$$$$$   4$$$$$*
      ^$$$B  $$$$\     $$$$$$$$$$$$   d$$R"
        "*$bd$$$$      '*$$$$$$$$$$$o+#"
             """"          """""""

 ________________________
< TASK [Gathering Facts] >
 ------------------------
 \     ____________
  \    |__________|
      /           /\
     /           /  \
    /___________/___/|
    |          |     |
    |  ==\ /== |     |
    |   O   O  | \ \ |
    |     <    |  \ \|
   /|          |   \ \
  / |  \_____/ |   / /
 / /|          |  / /|
/||\|          | /||\/
    -------------|
        | |    | |
       <__/    \__>

ok: [ansible-test]
 ___________________
< TASK [update git] >
 -------------------
  \   ^__^
   \  (oo)\_______        ________
      (__)\       )\/\    |Super |
          ||----W |       |Milker|
          ||    UDDDDDDDDD|______|

ok: [ansible-test]
 _______________
< TASK [uptime] >
 ---------------
        \    ,-^-.
         \   !oYo!
          \ /./=\.\______
               ##        )\/\
                ||-----w||
                ||      ||

               Cowth Vader

changed: [ansible-test]
 ______________
< TASK [debug] >
 --------------
       \    ____
        \  /    \
          | ^__^ |
          | (oo) |______
          | (__) |      )\/\
           \____/|----w |
                ||     ||

	         Moofasa

ok: [ansible-test] =>
  command_output.stdout_lines:
  - ' 11:39:43 up 10 days, 20:23,  0 users,  load average: 1.19, 1.29, 1.27'
 ____________
< PLAY RECAP >
 ------------
   \
    \
      _____   _________
     /     \_/         |
    |                 ||
    |                 ||
   |    ###\  /###   | |
   |     0  \/  0    | |
  /|                 | |
 / |        <        |\ \
| /|                 | | |
| |     \_______/   |  | |
| |                 | / /
/||                 /|||
   ----------------|
        | |    | |
        ***    ***
       /___\  /___\

ansible-test               : ok=4    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```


### O2

```
./run.sh ansible-test/setup.yml
 _____________________
< PLAY [ansible-test] >
 ---------------------
  \   ^__^
   \  (oo)\_______        ________
      (__)\       )\/\    |Super |
          ||----W |       |Milker|
          ||    UDDDDDDDDD|______|

 ________________________
< TASK [Gathering Facts] >
 ------------------------
       \    ____
        \  /    \
          | ^__^ |
          | (oo) |______
          | (__) |      )\/\
           \____/|----w |
                ||     ||

	         Moofasa

ok: [ansible-test]
 ___________________
< TASK [update git] >
 -------------------
  \
   \ ,   _ ___.--'''`--''//-,-_--_.
      \`"' ` || \\ \ \\/ / // / ,-\\`,_
     /'`  \ \ || Y  | \|/ / // / - |__ `-,
    /@"\  ` \ `\ |  | ||/ // | \/  \  `-._`-,_.,
   /  _.-. `.-\,___/\ _/|_/_\_\/|_/ |     `-._._)
   `-'``/  /  |  // \__/\__  /  \__/ \
        `-'  /-\/  | -|   \__ \   |-' |
          __/\ / _/ \/ __,-'   ) ,' _|'
         (((__/(((_.' ((___..-'((__,'

ok: [ansible-test]
 _______________
< TASK [uptime] >
 ---------------
    \                                  ___-------___
     \                             _-~~             ~~-_
      \                         _-~                    /~-_
             /^\__/^\         /~  \                   /    \
           /|  O|| O|        /      \_______________/        \
          | |___||__|      /       /                \          \
          |          \    /      /                    \          \
          |   (_______) /______/                        \_________ \
          |         / /         \                      /            \
           \         \^\\         \                  /               \     /
             \         ||           \______________/      _-_       //\__//
               \       ||------_-~~-_ ------------- \ --/~   ~\    || __/
                 ~-----||====/~     |==================|       |/~~~~~
                  (_(__/  ./     /                    \_\      \.
                         (_(___/                         \_____)_)

changed: [ansible-test]
 ______________
< TASK [debug] >
 --------------
  \
   \
      /\_)o<
     |      \
     | O . O|
      \_____/

ok: [ansible-test] =>
  command_output.stdout_lines:
  - ' 11:40:56 up 10 days, 20:24,  0 users,  load average: 0.97, 1.16, 1.22'
 ____________
< PLAY RECAP >
 ------------
  \     .    _  .
   \    |\_|/__/|
       / / \/ \  \
      /__|O||O|__ \
     |/_ \_/\_/ _\ |
     | | (____) | ||
     \/\___/\__/  //
     (_/         ||
      |          ||
      |          ||\
       \        //_/
        \______//
       __ || __||
      (____(____)

ansible-test               : ok=4    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

### O3

```
./run.sh ansible-test/setup.yml
 ____________
< PLAY [all] >
 ------------
       \   ,__,
        \  (oo)____
           (__)    )\
              ||--|| *

 ________________________
< TASK [Gathering Facts] >
 ------------------------
     \
      \
          oO)-.                       .-(Oo
         /__  _\                     /_  __\
         \  \(  |     ()~()         |  )/  /
          \__|\ |    (-___-)        | /|__/
          '  '--'    ==`-'==        '--'  '

ok: [ansible-test]
ok: [ansible-test2]
 ___________________
< TASK [update git] >
 -------------------
    \                                  ___-------___
     \                             _-~~             ~~-_
      \                         _-~                    /~-_
             /^\__/^\         /~  \                   /    \
           /|  O|| O|        /      \_______________/        \
          | |___||__|      /       /                \          \
          |          \    /      /                    \          \
          |   (_______) /______/                        \_________ \
          |         / /         \                      /            \
           \         \^\\         \                  /               \     /
             \         ||           \______________/      _-_       //\__//
               \       ||------_-~~-_ ------------- \ --/~   ~\    || __/
                 ~-----||====/~     |==================|       |/~~~~~
                  (_(__/  ./     /                    \_\      \.
                         (_(___/                         \_____)_)

ok: [ansible-test]
The following additional packages will be installed:
  git-man less libbrotli1 libcurl3-gnutls liberror-perl libgdbm-compat4
  libgdbm6 libldap-2.5-0 libldap-common libnghttp2-14 libperl5.34 librtmp1
  libsasl2-2 libsasl2-modules libsasl2-modules-db libssh-4 netbase patch perl
  perl-modules-5.34
Suggested packages:
  gettext-base git-daemon-run | git-daemon-sysvinit git-doc git-email git-gui
  gitk gitweb git-cvs git-mediawiki git-svn gdbm-l10n
  libsasl2-modules-gssapi-mit | libsasl2-modules-gssapi-heimdal
  libsasl2-modules-ldap libsasl2-modules-otp libsasl2-modules-sql ed
  diffutils-doc perl-doc libterm-readline-gnu-perl
  | libterm-readline-perl-perl make libtap-harness-archive-perl
The following NEW packages will be installed:
  git git-man less libbrotli1 libcurl3-gnutls liberror-perl libgdbm-compat4
  libgdbm6 libldap-2.5-0 libldap-common libnghttp2-14 libperl5.34 librtmp1
  libsasl2-2 libsasl2-modules libsasl2-modules-db libssh-4 netbase patch perl
  perl-modules-5.34
0 upgraded, 21 newly installed, 0 to remove and 0 not upgraded.
changed: [ansible-test2]
 _______________
< TASK [uptime] >
 ---------------
  \
   \   \_\_    _/_/
    \      \__/
           (oo)\_______
           (__)\       )\/\
               ||----w |
               ||     ||

changed: [ansible-test2]
changed: [ansible-test]
 ______________
< TASK [debug] >
 --------------
   \
    \
    ____
   /# /_\_
  |  |/o\o\
  |  \\_/_/
 / |_   |
|  ||\_ ~|
|  ||| \/
|  |||_
 \//  |
  ||  |
  ||_  \
  \_|  o|
  /\___/
 /  ||||__
    (___)_)

ok: [ansible-test] =>
  command_output.stdout_lines:
  - ' 11:48:09 up 10 days, 20:31,  0 users,  load average: 1.31, 1.05, 1.13'
ok: [ansible-test2] =>
  command_output.stdout_lines:
  - ' 11:48:09 up 10 days, 20:31,  0 users,  load average: 1.31, 1.05, 1.13'
 ____________
< PLAY RECAP >
 ------------
      \                    / \  //\
       \    |\___/|      /   \//  \\
            /0  0  \__  /    //  | \ \
           /     /  \/_/    //   |  \  \
           @_^_@'/   \/_   //    |   \   \
           //_^_/     \/_ //     |    \    \
        ( //) |        \///      |     \     \
      ( / /) _|_ /   )  //       |      \     _\
    ( // /) '/,_ _ _/  ( ; -.    |    _ _\.-~        .-~~~^-.
  (( / / )) ,-{        _      `-.|.-~-.           .~         `.
 (( // / ))  '/\      /                 ~-. _ .-~      .-~^-.  \
 (( /// ))      `.   {            }                   /      \  \
  (( / ))     .----~-.\        \-'                 .~         \  `. \^-.
             ///.----..>        \             _ -~             `.  ^-`  ^-_
               ///-._ _ _ _ _ _ _}^ - - - - ~                     ~-- ,.-~
                                                                  /.-~

ansible-test               : ok=4    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
ansible-test2              : ok=4    changed=2    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

### O4

```
./run.sh ansible-test/setup.yml

 ____________
< PLAY [all] >
 ------------
  \
   \
      /\_)o<
     |      \
     | O . O|
      \_____/

 ________________________
< TASK [Gathering Facts] >
 ------------------------
   \
    \        .
     .---.  //
    Y|o o|Y//
   /_(i=i)K/
   ~()~*~()~
    (_)-(_)

     Darth
     Vader
     koala

ok: [ansible-test2]
ok: [ansible-test]
 ___________________
< TASK [update git] >
 -------------------
\                             .       .
 \                           / `.   .' "
  \                  .---.  <    > <    >  .---.
   \                 |    \  \ - ~ ~ - /  /    |
         _____          ..-~             ~-..-~
        |     |   \~~~\.'                    `./~~~/
       ---------   \__/                        \__/
      .'  O    \     /               /       \  "
     (_____,    `._.'               |         }  \/~~~/
      `----.          /       }     |        /    \__/
            `-.      |       /      |       /      `. ,~~|
                ~-.__|      /_ - ~ ^|      /- _      `..-'
                     |     /        |     /     ~-.     `-. _  _  _
                     |_____|        |_____|         ~ - . _ _ _ _ _>

ok: [ansible-test2]
ok: [ansible-test]
 _______________
< TASK [uptime] >
 ---------------
 \     /\  ___  /\
  \   // \/   \/ \\
     ((    O O    ))
      \\ /     \ //
       \/  | |  \/
        |  | |  |
        |  | |  |
        |   o   |
        | |   | |
        |m|   |m|

changed: [ansible-test]
changed: [ansible-test2]
 ______________
< TASK [debug] >
 --------------
 \     ____________
  \    |__________|
      /           /\
     /           /  \
    /___________/___/|
    |          |     |
    |  ==\ /== |     |
    |   O   O  | \ \ |
    |     <    |  \ \|
   /|          |   \ \
  / |  \_____/ |   / /
 / /|          |  / /|
/||\|          | /||\/
    -------------|
        | |    | |
       <__/    \__>

ok: [ansible-test] =>
  command_output.stdout_lines:
  - ' 11:49:01 up 10 days, 20:32,  0 users,  load average: 0.86, 0.96, 1.09'
ok: [ansible-test2] =>
  command_output.stdout_lines:
  - ' 11:49:01 up 10 days, 20:32,  0 users,  load average: 0.86, 0.96, 1.09'
 ____________
< PLAY RECAP >
 ------------
  \
   \
       __
      UooU\.'@@@@@@`.
      \__/(@@@@@@@@@@)
           (@@@@@@@@)
           `YY~~~~YY'
            ||    ||

ansible-test               : ok=4    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
ansible-test2              : ok=4    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

## Comments

First I thought I wouldn't do this exerciese, since I've been using Ansible on 
VMs and didn't quite have the motivation to do this for a simple docker container.
However, as I saw many Dockerfile commands were written out in the assignment, 
I thought I'd give it a try. The exerciese was quite interesting.

The hardest was to get SSH working with key, until I realized I could just copy 
the ```authorized_keys``` into place. Also, for some reason I could run the 
run.sh without package 'Ansible' being installed on my Fedora. However, it 
didn't quite work before I installed the package.

I have been learning Ansible for a while now, for TiTes ATK-toimikunta and for 
myself (my servers). I'm not experienced yet, but I had a good base which I 
copied - that's why I also have used ansible-vault. 

I used ~4h for this exerciese.
