#!/bin/bash
set -euo pipefail
podman build -f Dockerfile -t ansible
podman rm --force ansible
podman rm --force ansible2

podman run -d                   \
    -p 2222:22                  \
    --name ansible              \
    localhost/ansible:latest

podman run -d                   \
    -p 2223:22                  \
    --name ansible2              \
    localhost/ansible:latest
