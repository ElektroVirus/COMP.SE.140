#!/bin/bash
set -euo pipefail

ansible-playbook -i inventory --vault-password-file .vaultpassword --diff "$@"

